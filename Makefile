include .env

db_directory=./mariadb-init

all: | build install info

install:
	@echo "Updating containers..."
	docker-compose pull
	@echo "Build and run containers..."
	docker-compose up -d
	docker-compose exec -T php composer global require -o --update-no-dev --no-suggest hirak/prestissimo
	docker-compose exec php composer install; \
	docker-compose exec php composer run drupal-scaffold
	make -s si

si:
	echo "Installing site $(PROFILE_NAME)"
	chmod +w docroot/sites/default/
	cp docroot/sites/default/default.settings.php docroot/sites/default/settings.php
	echo '$$config_directories["sync"] = "../config/sync";' >> docroot/sites/default/settings.php
	docker-compose exec -T php drush --root=/var/www/html/docroot site:install standard --db-url=mysql://$(PROFILE_NAME):$(PROFILE_NAME)@mariadb/$(PROFILE_NAME) --account-pass=admin --site-name="$(PROFILE_NAME)" -y --existing-config
	docker-compose exec -T php chmod -R 777 docroot/sites/default
#	docker-compose exec -T php sed -i 's/sites\/default\/files\/.*sync/..\/conf/g' docroot/sites/default/settings.php
	docker-compose exec -T php drush --root=/var/www/html/docroot user:password admin admin
	make -s chown;

chown:
	docker-compose exec -T php /bin/sh -c "chown $(shell id -u):$(shell id -g) /var/www/html -R"
	docker-compose exec -T php /bin/sh -c "chown www-data: /var/www/html/docroot/sites/default/files -R"

info:
ifeq ($(shell docker inspect --format="{{ .State.Running }}" $(PROFILE_NAME)_web 2> /dev/null),true)
	@echo Project IP: "localhost:"$(shell docker inspect --format='{{(index (index .NetworkSettings.Ports "80/tcp") 0).HostPort}}' $(PROFILE_NAME)_web)
endif

exec:
	docker-compose exec php bash

front:
	@echo "Building front tasks..."
	docker pull acidaniel/frontend:susy
	docker run --rm -v $(shell pwd)/src/$(PROFILE_NAME)/themes/$(THEME_NAME):/work acidaniel/frontend:susy bower install --allow-root
	docker run --rm -v $(shell pwd)/src/$(PROFILE_NAME)/themes/$(THEME_NAME):/work acidaniel/frontend:susy

phpcs:
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                acidaniel/php7:sniffers phpcs -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=php,module,inc,install,profile,theme,yml \
                --ignore=*.css,*.md,*.js,*.info.yml,bower_components/*,node_modules/*,css/*,images/*,styleguide/* .
phpcbf:
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                acidaniel/php7:sniffers phpcbf -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=php,module,inc,install,profile,theme,yml,txt,md \
                --ignore=*.css,*.md,*.js,*.info.yml,bower_components/*,node_modules/*,css/*,images/*,styleguide/* .

clean: info
	@echo "Removing networks for $(PROFILE_NAME)"
ifeq ($(shell docker inspect --format="{{ .State.Running }}" $(PROFILE_NAME)_web 2> /dev/null),true)
	docker-compose down
endif
