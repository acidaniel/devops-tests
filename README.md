## Requirements

* Docker - [Install](https://docs.docker.com/docker-for-mac/install/#download-docker-for-mac)

## Installation

  1. Fork or Clone this repository

  2. Rename the `.env-example` to `.env`
   * `PROFILE_NAME` - Project Name all in lowercase
   * `PROFILE_PORT` - On which Port you want your project running.
   * `THEME_NAME`   - Set this to the name of the theme `{PROFILE_NAME}_theme`.
   * `TRAEFIK_HOST` - The name you want for your local project (e.g. projec-local) without http or https.
   * `PROXY_HOST`   - In case you are behind a proxy like we are at Intel will be needed to set this variable (e.g http://proxy.com)
   * `PROXY_PORT`   - The Port of your proxies

  3. Edit your `/etc/hosts` with your `TRAEFIK_HOST` value.

  4. Go to the root of this project and run `make install` This will setup all containers for you.

## Styleguide

  1. Go to `http://PROFILE_NAME:PROFILE_PORT/modules/custom/{PROFILE_NAME}/themes/{PROFILE_NAME}_theme/styleguide/index.html`
     * Replace PROFILE_NAME and PROFILE_PORT with your values in the `.env` file*

  2. To rebuild the styleguide you migth want to create an alias, go to the theme path at `src/{PROFILE_NAME}/themes/${PROFILE_NAME}_theme` and run `alias front="docker run --rm -it -v $(pwd):/work acidaniel/frontend:susy"`

  3. Now you can run any gulp task *See gulpfile.js to see the list of all tasks* all you need to do is from your theme's path run `front gulp {TASK}` (e.g `front run compile:styleguide`).

### Styleguide recommendations

  1. Try to folow the BEM methodology to create your components.

    * Twig File:
      * ```
        <div class="card">
          <div class="cards__container">
            <div class="cards__items">
	      <div class-"card__items-title">
		Some title
	      </div>
	    </div>
          </div>
        </div>
	```

    * SCSS File:
      * ```
	.card {
	  &__container {

	  }
	  &__items {

	    &-title {

	    }
	  }
	}
	```

## Configuration and Backend workflow

### General Instructions

  1. All drush commands needs to be run within the container, to access to the container run `make exec` then `cd docroot` (leave that terminal opened).
  2. You need to edit the `settings.php` and remove the line `$settings['install_profile'] = 'minimal';` then add or replace the line `$config_directories['sync']` to `$config_directories['sync']='../conf'`
  3. Run `make si` to have your local environment with current configuration.
  4. Make your configuration changes (Content Types, Views, etc) and when you finish you need to export the configuration, then inside the container explained in step #1 run `drush cex -y` you need to ensure ONLY your changes are tracked.
  5. To import the configuration from other developer to yours (once it is merged in develop branch) just pull for latest changes and run inside the container explained in step #1 `drush cim -y`


### Export Database
  1. In the root of your project from your host machine NOT the container

  `docker-compose exec mariadb sh -c 'exec mysqldump -uroot -p"password" drupal' > my-db.sql`

## Disabling Cache for Local Development

  1. Create a file called `development.services.yml` in `docroot/sites/`
  2. Paste the following lines:

  ```
  # Local development services.
  #
  # To activate this feature, follow the instructions at the top of the
  # 'example.settings.local.php' file, which sits next to this file.
  parameters:
    http.response.debug_cacheability_headers: true
    twig.config:
      debug: true
      auto_reload: true
      cache: false
  services:
    cache.backend.null:
      class: Drupal\Core\Cache\NullBackendFactory
  ```
  3. In your `settings.php` put in the bottom of ther file the following code:

  ```
  $settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
  $settings['extension_discovery_scan_tests'] = FALSE;
  $settings['cache']['bins']['render'] = 'cache.backend.null';
  $settings['cache']['bins']['page'] = 'cache.backend.null';
  $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
  ```
  4. Clear your cache `drush cr all`
