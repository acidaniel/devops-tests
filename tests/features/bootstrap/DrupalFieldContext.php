<?php

/**
 * @file
 *
 * @copyright Copyright (c) 2017 Palantir.net
 */

use Behat\Gherkin\Node\TableNode;
use Palantirnet\PalantirBehatExtension\Context\SharedDrupalContext;

class DrupalFieldContext extends SharedDrupalContext
{
  /**
   * @var array Array[field-type => selector]
   * An array of field type selectors we know about.
   */
  private $fieldTypeSelectors = [
    'checkbox' => '.field--type-boolean .form-type-checkbox input.form-checkbox',
    'email' => ".form-type-email input.form-email",
    'link' => ".field--type-link .form-type-entity-autocomplete input.form-autocomplete",
    'name field' => ".field--type-name",
    'number' => '.form-number',
    'password' => '.js-form-type-password input.form-text',
    'reference' => ".form-type-entity-autocomplete input.form-autocomplete",
    'telephone' => ".field--type-telephone input.form-tel",
    'textarea' => ".form-type-textarea div.form-textarea-wrapper textarea",
    'textfield' => ".js-form-type-textfield input.form-text",
    'video embed' => ".field--type-video-embed-field input.form-text",
    'date' => ".field--type-datetime",
    'select' => ".form-select"
  ];

  /**
   * Asserts a page has fields provided in the form of a given type:
   * | field               | tag        | type  |
   * | title               | input      | text  |
   * | body                | textarea   |       |
   * | field-subheadline   | input      | text  |
   * | field-author        | input      | text  |
   * | field-summary       | textarea   |       |
   * | field-full-text     | textarea   |       |
   * | field-ref-sections  | select     |       |
   * | field-paragraph     | paragraphs |       |
   *
   * Assumes fields are targeted with #edit-<fieldname>. For example,
   * "body" checks for the existence of the element, "#edit-body". Note, for
   * almost everything this will begin with "field-", like "field-tags".
   *
   * @Then the form at :path has the expected fields:
   * @Then the content type :type has the expected fields:
   *
   * @param String $path
   * @param TableNode $fieldsTable
   * @param String $content_type
   * @throws \Exception
   */
  public function assertFields($path = '', $content_type = '', TableNode $fieldsTable) {
    // Load the page with the form on it.
    if (empty($path)) {
      $path = 'node/add/' . $content_type;
    }
    $this->getSession()->visit($this->locatePath($path));
    $page = $this->getSession()->getPage();

    foreach ($fieldsTable->getHash() as $row) {
      $fieldSelector = '#edit-' . $row['field'] . '-0-value';
      $page->hasField($fieldSelector);

      if (array_key_exists($row['tag'], $this->fieldTypeSelectors)) {
        $this->assertFieldType($row['field'], $row['tag'], $row['type']);
      }
      // Use a custom callback if it doesn't fit into the standard pattern.
      else {
        $callback = 'assert' . str_replace(['-', ' '], '', ucwords($row['tag']));
        if (!method_exists($this, $callback)) {
          throw new Exception(sprintf('%s is not a field we know how to validate.',
            $row['tag']));
        }
        $this->$callback('#edit-' . $row['field'], $row['type']);
      }

    }
  }

  /**
   * Test a field on the current page to see if it matches
   * the expected HTML field type.
   *
   * @Then the ":field" field is ":tag"
   * @Then the ":field" field is ":tag" with type ":type"
   *
   * @param string $field
   * @param string $type
   *
   * @throws Exception
   */
  public function assertFieldType($field, $type, $widget)
  {
    // Find all of the elements of this type on the page.
    $elements = $this->getSession()->getPage()->findAll('css', $this->fieldTypeSelectors[$type]);
    if (empty($elements)) {
      throw new Exception("Could not find any {$type} fields on the page.");
    } else {
      // There may be multiple, so loop through all of them.
      $found = FALSE;
      foreach ($elements as $element) {
        // Check to see if it is the field we are looking for.
        $id = $element->getAttribute('id');
        // Ignore the edit- prefix and the delta tacked on the end.
        if (substr($id, 5, strlen($field)) === $field) {
          $found = TRUE;
          break;
        }
      }
      if (FALSE === $found) {
        throw new Exception("Could not find the {$field} field of type {$type}.");
      }
    }
  }

  /**
   * Verify the field is a file field of the given type.
   *
   * @param $field
   * @param $expectedType
   * @throws Exception
   */
  public function assertFile($field, $expectedType) {
    $element = $this->getSession()->getPage()->find('css', $field . '-wrapper');
    if (NULL == $element || NULL == $element->find('css', 'input[type="file"]')) {
      throw new Exception(sprintf("Couldn't find %s of type %s", $field, $expectedType));
    }
  }

  /**
   * Verify the field is a paragraph field.
   *
   * @param $field
   * @param $expectedType
   * @throws Exception
   */
  public function assertParagraphs($field, $expectedType = '') {
    $element = $this->getSession()->getPage()->find('css', $field . '-wrapper');
    if (NULL == $element || NULL == $element->find('css', $field . '-add-more-add-more-button-' . $expectedType)) {
      throw new Exception(sprintf("Couldn't find %s of paragraph type %s", $field, $field . '-add-more-add-more-button-' . $expectedType));
    }
  }

  /**
   * Verify the field is an image field with the expected widget type.
   *
   * @param $field
   * @param $expectedType
   * @throws Exception
   */
  public function assertImage($field, $expectedType) {

    switch ($expectedType) {
      case 'entity-browser':
        $selector = 'input' . $field . '-' . $expectedType . '-target';
        $element = $this->getSession()->getPage()->find('css', $selector);
        if (empty($element)) {
          throw new Exception("Could not find field \"{$selector}\" on the page.");
        }
        break;
      default:
        throw new UnexpectedValueException("$expectedType is not an image widget we know how to validate. See DrupalFieldContext::assertImage() to add it.");
        break;
    };
  }
}
