<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext
{
    /** @var String The directory to save screenshots and html to. */
    private $debug_dir;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->debug_dir = getenv('CIRCLECI') ? getenv('CIRCLE_TEST_REPORTS') . '/' : __DIR__ . '/../../artifacts/debug/';
    }

    /**
     * @AfterStep
     *
     * @param \Behat\Behat\Hook\Scope\AfterStepScope $event
     */
    public function printLastResponseOnError(AfterStepScope $event)
    {
        if (!$event->getTestResult()->isPassed()) {
            $this->saveHtml();
        }
    }

    /**
     * Grab the html of the page and save it.
     *
     * @Then save the html for the page
     * @Then save the html for the page with prefix :prefix
     *
     * @param string $prefix A string to prepend to the filename.
     */
    public function saveHtml($prefix = 'html') {
        $html_data = $this->getSession()->getDriver()->getContent();
        $filename = $this->debug_dir . $prefix . '-' .  time() . '.html';
        file_put_contents($filename, $html_data);
    }

    /**
     * Grab a screenshot of the page and save it.
     *
     * @Then save a screenshot of the page
     * @Then save a screenshot of the page with prefix :prefix
     *
     * @param string $prefix A string to prepend to the filename.
     */
    public function saveAScreenshot($prefix = 'screenshot')
    {
        $screenshot = $this->getSession()->getDriver()->getScreenshot();
        $filename = $this->debug_dir . $prefix . '-' . time() . '.png';
        file_put_contents($filename, $screenshot);
    }
}
