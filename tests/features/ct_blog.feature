@api
Feature: Blog Content type
 As an administrator,
 I want to create blog content.

 Scenario: Validate the Blog content type has the expected fields.
   Given I am logged in as a user with the "administrator" role
   Then the form at "node/add/blog" has the expected fields:
     | field            | tag       | type |
     | title            | textfield | text |
     | body             | textarea  | text |
